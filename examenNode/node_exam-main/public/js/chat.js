$(document).ready(function () {

  var salaUrl = document.URL.slice(-1);
  if(parseInt(salaUrl)){
    $('#selectRoom').val(salaUrl)
  }
  else{
    salaUrl="all";
  }
  //Inicializa socket con IO
  //Cuando cambia el select redirigimos a la URL del chat
  $('#selectRoom').on("change",()=>{
    var sala = $(this).find("option:selected").val();
    window.location.href = "/chat/"+sala;
  })
  const socket = io();
  
  //Accion cuando el usuario envia mensaje con submit
  $("#chat").submit(function (e) {
    e.preventDefault();
    var msg = $("#msg").val();
    var user = $("#autor").val();
    $("#chatBox").append(`<p><strong>${user}</strong> : ${msg}<p>`);
    var toSend = {aut:user,msg:msg}
    socket.emit("newMsg"+salaUrl,toSend);
    $("#msg").val("");
  });

  if(salaUrl!="all"){
    socket.on("newMsg"+salaUrl,(data)=> {
      console.log(data);
      $('#chatBox').append('<p><strong>'+data.aut+'</strong> : '+ data.msg +'</p>');
      console.log(data);
    }) 
  }
  socket.on("newMsgall",(data)=> {
    console.log(data);
    $('#chatBox').append('<p><strong>'+data.aut+'</strong> : '+ data.msg +'</p>');
    console.log(data);
  })
});