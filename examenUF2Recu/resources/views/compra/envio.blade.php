@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
  <div class="col-md-8">
    <div class="card">
      <div class="card-header">{{ __('Register') }}</div>

      <div class="card-body">
      @foreach ($errors->all() as $error)
        @if($error == 'No se puede guargar la imagen: permisos insuficientes' || $error == 'No se pudo guargar la imagen' || $error == 'Espacio insuficiente para guardar la imagen!')
          <div class="col offset-3">
            <h4 style="color:Red;">{{ $error }}<h4>
          </div>
        @endif
      @endforeach

        <form method="POST" enctype="multipart/form-data" action="{{ action('CompraController@verificarEnvio') }}">
          @csrf

          <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

            <div class="col-md-6">
              <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name', '') }}" autocomplete="name" autofocus>
              @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>Nombre requerido</strong>
              </span>
              @enderror
            </div>
          </div>

          <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

            <div class="col-md-6">
              <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email', '') }}" autocomplete="email">
            
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>Email requerido</strong>
              </span>
              @enderror
              </div>
          </div>

          <div class="form-group row">
            <label for="location" class="col-md-4 col-form-label text-md-right">{{ __('Location') }}</label>

            <div class="col-md-6">
              <input id="location" type="text" class="form-control @error('location') is-invalid @enderror" name="location" value="{{ old('location', '') }}" autocomplete="email">
            
            @error('location')
            <span class="invalid-feedback" role="alert">
                <strong>Direccion requerido</strong>
              </span>
              @enderror
              </div>
          </div>

          <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

            <div class="col-md-6">
              <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password">
            
            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>Contraseña requerida</strong>
              </span>
              @enderror
              </div>
          </div>

          <div class="form-group row">
            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

            <div class="col-md-6">
              <input id="password-confirm" type="password" class="form-control @error('password-confirm') is-invalid @enderror" name="password-confirm" autocomplete="new-password">
            
            @error('password-confirm')
            <span class="invalid-feedback" role="alert">
                <strong>Confirmacion requerido</strong>
              </span>
              @enderror
              </div>
          </div>

          <div class="form-group row">
            <label for="foto" class="col-md-4 col-form-label text-md-right">{{ __('Foto') }}</label>

            <div class="col-md-6">
              <input id="foto" type="file" class="form-control @error('foto') is-invalid @enderror" accept="image/*" name="foto">
              @error('foto')
            <span class="invalid-feedback" role="alert">
                <strong>Fotografia requerida</strong>
              </span>
              @enderror
            </div>
          </div>
          <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
              <button type="submit" class="btn btn-primary">
                {{ __('Pagar') }}
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<a href="{{ url('/compra/resumen') }}" class="btn btn-secondary btn-lg float-left">Atras</a>
<a href="{{ url('/compra/confirmar') }}"  class="btn btn-primary btn-lg float-right">Siguiente</a>
<!--<a href="{{ url('/compra/confirmar') }}"  class="btn btn-primary btn-lg float-right">Siguiente</a>-->

<br><br>
@endsection