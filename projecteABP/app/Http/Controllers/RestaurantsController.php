<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Restaurant;
use App\Http\Controllers\Controller;

class RestaurantsController extends Controller
{
    public $restaurantsModel;
    public function __construct() {
        $this->restaurantsModel = new Restaurant();
    }
    public function list(){
        return view('restaurants')->with(['restaurants'=>$this->restaurantsModel->all()]);
    }
    public function listSearch(Request $request){
        return view('restaurants')->with(['restaurants'=>$this->restaurantsModel->search($request->input('search'))]);
    }
    public function listTipus(Request $request){
        return view('restaurants')->with(['restaurants'=>$this->restaurantsModel->search($request->input('type'))]);
    }
    public function listMenus(){
        return view('restaurants')->with(['restaurants'=>$this->restaurantsModel->menus()]);
    }
    public function listLotes(){
        return view('restaurants')->with(['restaurants'=>$this->restaurantsModel->lotes()]);
    }
    public function listDescomptes(){
        return view('restaurants')->with(['restaurants'=>$this->restaurantsModel->descomptes()]);
    }
}