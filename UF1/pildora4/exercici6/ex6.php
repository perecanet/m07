<html>
  <head>
    <title>Ex6</title>
    <link rel="stylesheet" href="estil.css">
  </head>
  <body>
    <?php
        function intermedio($a, $b) {
            $intermig = ($a + $b) / 2;
            return $intermig;
        }
        $num1 = -12;
        $num2 = 24;
        $resultat = intermedio($num1, $num2);
        echo $resultat;
    ?>
  </body>
</html>