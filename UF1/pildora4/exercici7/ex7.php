<html>
  <head>
    <title>Ex7</title>
  </head>
  <body>
    <?php
        function separar($array) {
            $l_parells = array();
            $l_senars = array();
            foreach ($array as $i){
                if (($i % 2) != 0 ) {
                    $l_senars[] = $i;
                } else {
                    $l_parells[] = $i;
                }
            }
            sort($l_parells);
            sort($l_senars);
            return array($l_parells, $l_senars);
        }
        $l_nombres = [-12, 84, 13, 20, -33, 101, 9];
        $resultat = separar($l_nombres);
        print_r($resultat);
    ?>
  </body>
</html>