<?php

namespace App\Exceptions;

use Exception;

class PriceTypeException extends Exception
{
    public function customMessage(){
        return "El camp 'preu' ha de ser numèric";
    }
}