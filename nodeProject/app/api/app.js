var path = require("path");
var ctrlDir = "/app/app/controllers";
var express = require("express");
var router = express.Router();

var chatCtrl = require(path.join(ctrlDir,"chat"));
var incidenciesCtrl = require(path.join(ctrlDir,"incidencies"));

router.get("/chat/load", chatCtrl.load);
router.get("/incidencia", incidenciesCtrl.loadIncidencies);

router.post("/incidencies/nova", (req, res, next) => {
    api.call("/incidencies/loadIncidencies")
        .then(function (val) {
            res.render("incidencies", val);
        })
        .catch(function (err) {
            console.log(err);
        });
    console.log("se esta procesando");
});
module.exports = router;
