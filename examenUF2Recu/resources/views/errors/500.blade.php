@extends((Request::ajax()) ? 'layouts.ajax' : 'layouts.app')

@section('content')
<div class="content">
    <div class="row">
        <h1>Lo sentimos, ha habido un error con el servidor.</h1>
    </div>
</div>
@endsection