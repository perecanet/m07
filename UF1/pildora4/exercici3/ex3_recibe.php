<html>
  <head>
    <title>Ex3</title>
    <link rel="stylesheet" href="estil.css">
  </head>
  <body>
    <?php
        function volum ($radiBase, $alturaCilindre) {
            $volumTotal = 3.1416 * $radiBase * $radiBase * $alturaCilindre;
            return $volumTotal;
        }
        $radi = $_POST["radi"];
        $altura = $_POST["altura"];
        echo volum($radi, $altura);
    ?>
  </body>
</html>