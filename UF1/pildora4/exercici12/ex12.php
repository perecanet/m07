<html>
  <head>
    <title>Ex12</title>
  </head>
  <body>
    <?php
        class Tabla {
            private $mat=array();
            private $numFiles;
            private $numColumnes;
            public function __construct($fi,$co)
            {
                $this->numFiles=$fi;
                $this->numColumnes=$co;
            }
            
            public function cargar($fila,$columna,$valor)
            {
                $this->mat[$fila][$columna]=$valor;
            }
            
            private function inicioTabla()
            {
                echo '<table border="1">';
            }
            
            private function inicioFila()
            {
                echo '<tr>';
            }
            
            private function mostrar($fi,$co)
            {
                echo '<td>'.$this->mat[$fi][$co].'</td>';
            }
            
            private function finFila()
            {
                echo '</tr>';
            }
            
            private function finTabla()
            {
                echo '</table>';
            }
            
            public function graficar()
            {
                $this->inicioTabla();
                for($f=1;$f<=$this->numFiles;$f++)
                {
                  $this->inicioFila();
                  for($c=1;$c<=$this->numColumnes;$c++)
                  {
                    $this->mostrar($f,$c);
                  } 
                  $this->finFila();
                }
                $this->finTabla();            }
        }
        
        $tabla1=new Tabla(3,3);
        $tabla1->cargar(1,1,"1");
        $tabla1->cargar(1,2,"2");
        $tabla1->cargar(1,3,"3");
        $tabla1->cargar(2,1,"4");
        $tabla1->cargar(2,2,"5");
        $tabla1->cargar(2,3,"6");
        $tabla1->cargar(3,1,"7");
        $tabla1->cargar(3,2,"8");
        $tabla1->cargar(3,3,"9");
        $tabla1->graficar();
    ?>
  </body>
</html>