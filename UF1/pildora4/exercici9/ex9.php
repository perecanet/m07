<html>
  <head>
    <title>Ex9</title>
  </head>
  <body>
   <?php
    class Menu {
        private $links=array();
        private $noms=array();
        public function cargarOpcion($enllaç,$titol)
        {
          $this->links[]=$enllaç;
          $this->noms[]=$titol;
        }
        public function mostrar()
        {
          $arrayLinks = $this->links;
          $arrayNoms = $this->noms;
          ?><table style="width:100%">
          <tr>
            <?php for ($i = 0 ; $i < count($arrayLinks) ; $i++) {
              ?><td><a href="<?php echo $arrayLinks[$i]; ?>"><?php echo $arrayNoms[$i]; ?></a></td>
              <?php
            }
        }        
    }
    
    $menu=new Menu();
    $menu->cargarOpcion('https://kaosenlared.net','Kaos en la red');
    $menu->cargarOpcion('https://directa.cat/','La Directa');
    $menu->cargarOpcion('https://www.versembrant.cat/','Versembrant');
    $menu->mostrar();
   ?>
  </body>
</html>