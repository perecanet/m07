<html>
  <head>
    <title>Ex10</title>
  </head>
  <body>
   <?php
    class CabeceraPagina {
        private $titulo;
        private $posicion;
        private $color;
        public function inicializar($title,$position,$colour)
        {
          $this->titulo=$title;
          $this->posicion=$position;
          $this->color=$colour;
        }
        public function mostrar()
        {
          $titol = $this->titulo;
          $posicio = $this->posicion;
          $colorFons = $this->color;
          ?><h1 style="color:<?php echo $colorFons; ?>; text-align:<?php echo $posicio; ?>"><?php echo $titol; ?></h1><?php
        }        
    }
    $styleBlock = sprintf('
    <style type="text/css">
       body {
         background-color:%s
       }
    </style>
  ', $_POST["fons"]);
  echo $styleBlock;
    $header=new CabeceraPagina();
    $header->inicializar($_POST["titol"],$_POST["posicio"],$_POST["font"]);
    $header->mostrar();
   ?>
  </body>
</html>