<html>
  <head>
    <title>Exercici 2</title>
  </head>
  <body>
    <?php
    $num1 = 4;
    $num2 = 3;
    $suma = $num1 + $num2;
    $resta = $num1 - $num2;
    $multiplicacio = $num1 * $num2;
    $divisio = $num1 / $num2;
    $exponent = $num1 ** $num2;
    echo '<table style="width:100%">
    <tr>
     <th>Operacio</th>
     <th>Valor</th>
    </tr>
    <tr>
     <td>A</td>
     <td>$num1</td>
    </tr>
    <tr>
     <td>B</td>
     <td>$num2</td>
    </tr>
    <tr>
     <td>A+B</td>
     <td>$suma</td>
    </tr>
    <tr>
     <td>A-B</td>
     <td>$resta</td>
    </tr>
    <tr>
     <td>A*B</td>
     <td>$multiplicacio</td>
    </tr>
    <tr>
     <td>A/B</td>
     <td>$divisio</td>
    </tr>
    <tr>
     <td>AexpB</td>
     <td>$exponent</td>
    </tr>';
    ?>
  </body>
</html>