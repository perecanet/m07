<?php
namespace App\Http\Controllers;

use App\Exceptions\PriceTypeException;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Services\UploadFileService;
use App\Exceptions\UploadFileException;
use Illuminate\Support\Facades\Validator;

class ProductsController extends Controller
{
    private $product;
    private $uploadService;
    private $error = '';
    private $priceService;

    public function __construct()
    {
        $this->product = new Product();
    }

    public function addProduct(){
        return view('newProduct');
    }
    
    public function storeProduct(Request $request, UploadFileService $UploadFileService)
    {
        $success = false;
        $request->validate([
            'name' => 'required|unique:products',
            'price' => 'required',
            'category' => 'required',
            'description' => 'required',
        ]);
        try {
            
            $this->uploadService = $UploadFileService;
            $this->uploadService->uploadFile($request->file('image'));
            
            $this->priceService = $UploadFileService;
            $this->priceService->checkPrice($request->input('price'));

            $product = new Product;
            $product->name = $request->input('name');
            $product->price  = $request->input('price');
            $product->category  = $request->input('category');
            $product->description  = $request->input('description');
            $product->image  = time().$request->image->getClientOriginalName();
            $success = $product->save();

        } catch (UploadFileException $exception) {
            $this->error = $exception->customMessage();
        } catch (PriceTypeException $exception) {
            $this->error = $exception->customMessage();
        } catch ( \Illuminate\Database\QueryException $exception) {
            $this->error = "Ups.. Hi ha hagut un error amb les dades introduïdes";
        }
        return redirect()->action([ProductsController::class, 'addProduct'], ['success' => $success])->withError($this->error);

    }

    
}


/*public function storeProduct(Request $request, UploadFileService $UploadFileService) {
        
        $request->validate([
            'name' => 'required|unique:products',
            'price' => 'required',
            'category' => 'required',
            'description' => 'required',
            'image' => 'required'
        ]);
        
        try {

            $this->uploadService = $UploadFileService;
            $this->uploadService->uploadFile($request->file('image'));

            $product = new Product;
            $product->name = $request->input('name');
            $product->price = $request->input('price');
            $product->category = $request->input('category');
            $product->description = $request->input('description');
            $product->image = $request->image->getClientOriginalName();
            $product->save();
        } catch (UploadFileException $exception) {
            $this->error = $exception->customMessage();
        } catch ( \Illuminate\Database\QueryException $exception) {
            $this->error = "Error amb les dades introduïdes";
        }
        
        return back()->with('productAdded', 'Producte afegit exitosament!')->withError($this->error);;
    }*/