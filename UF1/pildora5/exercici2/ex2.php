<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Ex2</title>
    </head>
    <body>
        <?php

        // Check if the form was submitted
        if($_SERVER["REQUEST_METHOD"] == "POST"){
            $documents = $_FILES["fichero_usuario"];
            for ($i = 0 ; $i < count($documents["name"]) ; $i++) {
                // Check if file was uploaded without errors
                if(isset($_FILES["fichero_usuario"]) && $_FILES["fichero_usuario"]["error"][$i] == 0){
                    $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
                    $filename = $_FILES["fichero_usuario"]["name"][$i];
                    $filetype = $_FILES["fichero_usuario"]["type"][$i];
                    $filesize = $_FILES["fichero_usuario"]["size"][$i];
                
                    // Verify file extension
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    if(!array_key_exists($ext, $allowed)) die("Error: Please select a valid file format.");
                
                    // Verify file size - 5MB maximum
                    $maxsize = 5 * 1024 * 1024;
                    if($filesize > $maxsize) die("Error: File size is larger than the allowed limit.");
                
                    // Verify MYME type of the file
                    if(in_array($filetype, $allowed)){
                        // Check whether file exists before uploading it
                        if(file_exists("upload/" . $filename)){
                            echo $filename . " already exists.";
                            echo "<br />";
                            echo "Nom del fitxer: " . $filename;
                            echo "<br />";
                            echo "Tipus del fitxer: " . $filetype;
                            echo "<br />";
                            echo "Extensió: " . $ext;
                            echo "<br />";
                            echo "Tamany: " . $filesize;
                        } else{
                            move_uploaded_file($_FILES["fichero_usuario"]["tmp_name"], "upload/" . $filename);
                            echo "Your file was uploaded successfully.";
                            echo "<br />";
                            echo "Nom del fitxer: " . $filename;
                            echo "<br />";
                            echo "Tipus del fitxer: " . $filetype;
                            echo "<br />";
                            echo "Extensió: " . $ext;
                            echo "<br />";
                            echo "Tamany: " . $filesize;
                        } 
                    } else{
                        echo "Error: There was a problem uploading your file. Please try again."; 
                    }
                } else{
                    echo "Error: " . $_FILES["fichero_usuario"]["error"][$i];
                }
                echo "<br />";
                echo "<br />";
                echo "<br />";
            }
            
        }
        ?>
    </body>
</html>