<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>File Upload Form</title>
    </head>
    <body>
        <?php
        $directori = "<div class=""></div>subidas/";
        if(!is_dir($directori) {
            mkdir($directori, 0777);
        }
        // Check if the form was submitted
        if($_SERVER["REQUEST_METHOD"] == "POST"){
            // Check if file was uploaded without errors
            // Move the uploaded file
            move_uploaded_file($_FILES["photo"]["tmp_name"], $directori . $_FILES["photo"]["name"]);

            if(isset($_FILES["photo"]) && $_FILES["photo"]["error"] == 0){
                $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
                $filename = $_FILES["photo"]["name"];
                $filetype = $_FILES["photo"]["type"];
                $filesize = $_FILES["photo"]["size"];
            
                // Verify file extension
                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                if(!array_key_exists($ext, $allowed)) die("Error: Please select a valid file format.");
            
                // Verify file size - 5MB maximum
                $maxsize = 5 * 1024 * 1024;
                if($filesize > $maxsize) die("Error: File size is larger than the allowed limit.");
                
                if(in_array($filetype, $allowed)){
                    // Check whether file exists before uploading it
                    if(file_exists("/home/runner/" . $directori . $filename)){
                        echo "File already exists!";
                    } else {
                        move_uploaded_file($_FILES["photo"]["tmp_name"], "/home/runner/" . $directori . $filename);
                    }
            } else{
                echo "Error: " . $_FILES["photo"]["error"];
            }
        }
        $fitxers = scandir("/home/runner/subidas");
        print_r($fitxers);
        ?>
    </body>
</html>