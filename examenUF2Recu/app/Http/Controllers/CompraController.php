<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Exception;
use App\Services\SpaceService;
use App\Exceptions\SpaceException;


class CompraController extends Controller
{
    private $SpaceService;

    public function main()
    {
        /*Recuerda el estado de la compra y redirige a la pantalla en la que el usuario estaba antes: resumen, envio o confirmar */
        if(session('estadocomp') == 'confirm'){
            return redirect('/compra/confirmar');
        }
        elseif(session('estadocomp') == 'envio'){
            return redirect('/compra/envio');
        }
        else{
            return redirect('/compra/resumen');
        }
    }
    /**
     * Method to show the resume of the products in the chart
     */
    public function resumen()
    {
        session(['estadocomp' => 'resum']);
        $carrito = app('request')->session()->get('carrito',[]);
        $products = Product::query();
        $products->getproduct($carrito);
        $productcount = array_count_values($carrito);
        return view('compra/resumen')
            ->with('products', $products->get())
            ->with('productscount', $productcount);
    }

    /**
     * Method to show and process the shipping form (envio)
     */
    public function envio()
    {
        session(['estadocomp' => 'envio']);

        return view('compra/envio');
    }
    /**
     * Method to show and process the shipping form (envio)
     */
    public function verificarEnvio(Request $request, SpaceService $spaceService)
    {
        $request->session()->put('name', "");
        $request->session()->put('email', "");
        $request->session()->put('location', "");
        $request->session()->put('password', "");
        $formOK = false;

/* PONER AQUI TODO LO NECESARIO PARA VERIFICAR EL FORMULARIO */
        $request->validate([
            'name' => 'required|max:30',
            'email' => 'required',
            'location' => 'required',
            'password' => 'required|same:password-confirm',
            'password-confirm' => 'required',
            'foto' => 'required|image'
        ]);
        
        try {
            $this->SpaceService = $spaceService;
            $this->SpaceService->spaceleft($request->file('foto'));

            $request->session()->put('name', $request->input('name'));
            $request->session()->put('email', $request->input('email'));
            $request->session()->put('location', $request->input('location'));
            $request->session()->put('password', $request->input('password'));

            $fileName = time().$request->file('foto')->getClientOriginalName();
            $request->file('foto')->move(public_path().'/img/users/', $fileName);
            $request->session()->put('foto', $fileName);
            
            $formOK = true;
        } catch (SpaceException $e) {
            $this->error = $e->customMessage();
        }
        //si el formulario se ha rellenado correctamente se redirecciona a la pagina de confirmación
        if($formOK) {
            return redirect('/compra/confirmar');
        }
        return redirect('/compra/envio');
    }
    /**
     * Method to show the list of procuts and shipping info
     */
    public function confirmar()
    {
        session(['estadocomp' => 'confirm']);
        
        $carrito = app('request')->session()->get('carrito',[]);
        $products = Product::query();
        $products->getproduct($carrito);
        $productcount = array_count_values($carrito);
        //Dummy: hay que cambiar la info por la información guardada en session
        $shipping =   (object)[
            'name' => app('request')->session()->get('name'), 
            'mail' => app('request')->session()->get('email'),
            'address' => app('request')->session()->get('location'),
            'image' => app('request')->session()->get('foto'),
        ];
        return view('compra/confirmar')->with('products', $products->get())->with('shipping', $shipping)->with('productscount', $productcount);  
    }

    public function final(){
        app('request')->session()->put('carrito',[]);
        return view('compra/final');
    }
}
