var express = require("express");
var router = express.Router();
var api = require("/app/app/api/request");

router.get("/", (req, res, next) => {
    api.call("/chat/load")
        .then(function (val) {
            res.render("chat", val);
        })
        .catch(function (err) {
            console.log(err);
        });
    console.log("se esta consultando");
});

module.exports = router;