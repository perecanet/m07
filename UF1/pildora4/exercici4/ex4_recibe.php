<html>
  <head>
    <title>Ex4</title>
    <link rel="stylesheet" href="estil.css">
  </head>
  <body>
    <?php
        function descompte($total){
            $descompte = 0;
            settype($total, "integer");
            if ($total >= 100 && $total < 500) {
                $descompte = $total * 0.1;
            } else if ($total >= 500) {
                $descompte = $total * 0.15;
            }
            $total = $total - $descompte;
            return $total;
        }
        $preu = $_POST["preu"];
        echo ("El preu final de la compra, aplicant els descomptes és de " . descompte($preu));
    ?>
  </body>
</html>