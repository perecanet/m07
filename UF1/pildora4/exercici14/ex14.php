<html>
  <head>
    <title>Ex14</title>
  </head>
  <body>
   <?php
    class CabeceraDePagina {
        private $titulo;
        private $posicion;
        private $color;
        public function __construct($title,$position,$colour)
        {
          $this->titulo=$title;
          $this->posicion=$position;
          $this->color=$colour;
        }
        public function graficar()
        {
          $titol = $this->titulo;
          $posicio = $this->posicion;
          $colorFons = $this->color;
          ?><h1 style="color:<?php echo $colorFons; ?>; text-align:<?php echo $posicio; ?>"><?php echo $titol; ?></h1><?php
        }        
    }
    $styleBlock = sprintf('
    <style type="text/css">
       body {
         background-color:%s
       }
    </style>
  ', $_POST["fons"]);
  echo $styleBlock;
    $header=new CabeceraDePagina($_POST["titol"],$_POST["posicio"],$_POST["font"]);
    $header->graficar();
   ?>
  </body>
</html>