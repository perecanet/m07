<div class="search-product">
    <form role="form" id="filterSearch" method="post">
        <input class="form-control" placeholder="Cerca" type="text" name="search">
        <button class="btn hvr-hover " type="submit">Cercar <i class="fa fa-search" aria-hidden="true"></i></button>
        {{ csrf_field() }}
    </form>
</div>
<br />
<div class="filter-sidebar-left">
    <div class="title-left">
        <h3>Filtres</h3>
    </div>
    <div class="list-group list-group-collapse list-group-sm list-group-tree" id="list-group-men" data-children=".sub-men">
        <div class="list-group-collapse sub-men">
            <a class="list-group-item list-group-item-action" href="#sub-men1" data-toggle="collapse" aria-expanded="false" aria-controls="sub-men1">Tipus de menjar</a>
            <div class="collapse show" id="sub-men1" data-parent="#list-group-men">
                <div class="list-group">
                    <a href="{{ action([App\Http\Controllers\RestaurantsController::class, 'listTipus'], ['type'.'='.'Americà']) }}" class="list-group-item list-group-item-action">Americà <small class="text-muted"></small></a>
                    <a href="{{ action([App\Http\Controllers\RestaurantsController::class, 'listTipus'], ['type'.'='.'Hamburgueses']) }}" class="list-group-item list-group-item-action">Hamburgueses <small class="text-muted"></small></a>
                    <a href="{{ action([App\Http\Controllers\RestaurantsController::class, 'listTipus'], ['type'.'='.'Italià']) }}" class="list-group-item list-group-item-action">Italià <small class="text-muted"></small></a>
                    <a href="{{ action([App\Http\Controllers\RestaurantsController::class, 'listTipus'], ['type'.'='.'Japonès']) }}" class="list-group-item list-group-item-action">Japonès <small class="text-muted"></small></a>
                    <a href="{{ action([App\Http\Controllers\RestaurantsController::class, 'listTipus'], ['type'.'='.'Vegetarià']) }}" class="list-group-item list-group-item-action">Vegetarià <small class="text-muted"></small></a>
                    <a href="{{ action([App\Http\Controllers\RestaurantsController::class, 'listTipus'], ['type'.'='.'Mexica']) }}" class="list-group-item list-group-item-action">Mexica <small class="text-muted"></small></a>
                </div>
            </div>
        </div>
        <a href="{{ action([App\Http\Controllers\RestaurantsController::class, 'listDescomptes']) }}" class="list-group-item list-group-item-action"> Descomptes  <small class="text-muted"></small></a>
        <a href="{{ action([App\Http\Controllers\RestaurantsController::class, 'listMenus']) }}" class="list-group-item list-group-item-action"> Menús <small class="text-muted"></small></a>
        <a href="{{ action([App\Http\Controllers\RestaurantsController::class, 'listLotes']) }}" class="list-group-item list-group-item-action"> Lotes <small class="text-muted"></small></a>
    </div>
</div>

<script>
    $('#filterSearch').submit(function(e) {
        e.preventDefault()
        var data = $('#filterSearch').serialize()
        axios.post('restaurants',data)
            .then(response => {
                console.log(response)
                $('#content').replaceWith(response.data)
            })
    })
</script>
