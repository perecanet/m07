<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function scopeCategory($query,$input){
        return $query->where('category','=',$input);
    }
    public function scopeRating($query,$input){
        return $query->whereIn('rating',$input);
    }
    public function scopeGetproduct($query,$input){
        return $query->whereIn('id',$input);
    }
    public function scopeGetoneproduct($query,$input,$qty){
        return $query->where("id","=",$input)->where('stock',">=",$qty);
    }
}
