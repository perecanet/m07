<html>
  <head>
    <title>Ex8</title>
  </head>
  <body>
    <?php
        class Empleado {
            private $nombre;
            private $sueldo;
            public function inicializar($nom, $sou)
            {
              $this->nombre=$nom;
              $this->sueldo=$sou;
            }
            public function imprimir()
            {
              if ($this->sueldo >= 3000) {
                  echo ($this->nombre . " debe pagar impuestos");
              } else {
                  echo ($this->nombre . " no debe pagar impuestos");
              }
            }
          }
        $currante = new Empleado();
        $currante->inicializar('Pere', 3900);
        $currante->imprimir();
    ?>
  </body>
</html>