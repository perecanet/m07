<html>
  <head>
    <title>Ex4</title>
    <link rel="stylesheet" href="estil.css">
  </head>
  <body>
    <?php
        $array;
        $n_jugadors = $_POST["n_jugadors"];
        $n_partits = $_POST["n_partits"];
        for ($i = 1 ; $i <= $n_jugadors ; $i++) {
          $array2 = array(); 
          $jugador = $_POST["jugador".$i.""];
          $array2[] = $jugador;
          for ($j = 1 ; $j <= $n_partits; $j++) {
            $gols = $_POST["jugador".$i."partit".$j.""];
            $array2[] = $gols;
          }
          $array[] = $array2;
        }
    ?>
    <table style="width:100%">
      <tr>
        <?php for ($i = 0; $i <= $n_partits; $i++){
          if ($i==0){
            ?><th>Jugadors</th><?php
          } else {
            ?><th>Gols Partit<?php echo $i; ?></th><?php
          }
        } ?>
      </tr>
      <?php foreach ($array as $llista) {?>
        <tr><?php
          for ($i=0; $i<=$n_partits; $i++) {?>
            <td><?php echo $llista[$i]; ?></td><?php
          }?>
        </tr><?php 
      }?>
    </table>
  </body>
</html>