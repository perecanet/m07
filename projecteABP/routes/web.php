<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RestaurantsController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\CistellaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('restaurants');
});

Route::get('restaurants', [RestaurantsController::class, 'list']);
Route::post('restaurants', [RestaurantsController::class, 'listSearch']);
Route::get('restaurants/descomptes', [RestaurantsController::class, 'listDescomptes']);
Route::get('restaurants/menus', [RestaurantsController::class, 'listMenus']);
Route::get('restaurants/lotes', [RestaurantsController::class, 'listLotes']);
Route::get('restaurants/tipus', [RestaurantsController::class,'listTipus']);

Route::get('nouproducte', [ProductsController::class,'addProduct']);
Route::post('nouproducte', [ProductsController::class,'storeProduct'])->name('product.store');

Route::get('cistella', [CistellaController::class, 'show']);