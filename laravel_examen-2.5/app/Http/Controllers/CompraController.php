<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompraController extends Controller
{

    public function main()
    {
        /*Recuerda el estado de la compra y redirige a la pantalla en la que el usuario estaba antes: resumen, envio o confirmar */
        return redirect('/compra/resumen');
    }
    /**
     * Method to show the resume of the products in the chart
     */
    public function resumen()
    {
        //Dummy: hay que cambiar la info por la información guardada en el carrito (session)
        $products = app('request')->session()->get('carrito',[]);
        $total = count(app('request')->session()->get('carrito',[]));
        
        //$('#carrito').html({{ count(app('request')->session()->get('carrito',[])) }});
        
        return view('compra/resumen')
            ->with('products', $products);
    }

    /**
     * Method to show and process the shipping form (envio)
     */
    public function envio()
    {
        return view('compra/envio');
    }
    /**
     * Method to show and process the shipping form (envio)
     */
    public function verificarEnvio(Request $request)
    {
        $formOK = false;

        /* PONER AQUI TODO LO NECESARIO PARA VERIFICAR EL FORMULARIO */
        $request->validate([
            'name' => 'required|max:25',
            'email' => 'required',
            'location' => 'required',
            'password' => 'required',
            'password-confirm' => 'required',
            'foto' => 'required'
        ]);
        try {
            $request->session()->put('name', $request->input('name'));
            $request->session()->put('email', $request->input('email'));
            $request->session()->put('location', $request->input('location'));
            $request->session()->put('password', $request->input('password'));
            $request->session()->put('password-confirm', $request->input('password-confirm'));
            
            $fileName = time().$request->file('foto')->getClientOriginalName().$file->extension();
            $file->move(public_path().'/images/', $fileName);
            $request->session()->put('foto', $fileName);
            $formOK = true;
        } catch (\Illuminate\Database\QueryException $exception) {
            $this->error = "Error amb les dades introduïdes";
            return 'Error';
        }
        //si el formulario se ha rellenado correctamente se redirecciona a la pagina de confirmación
        if ($formOK) redirect('/compra/confirmar');
        return view('compra/envio');
    }
    /**
     * Method to show the list of procuts and shipping info
     */
    public function confirmar()
    {
        //Dummy: hay que cambiar la info por la información guardada en session
        $products = [
            (object) [
                'name' => 'Lego 1', 'category' => 0, 'description' => 'Esto es la descripcion lego 1', 'price' => 10.02, 'image' => 'lego1.jpeg', 'rating' => 2
            ]
        ];
        //Dummy: hay que cambiar la info por la información guardada en session
        $shipping =   (object)[
            'name' => 'Pedro', 
            'mail' => 'asds|@asda.es',
            'address' => 'asds|@asda.es',
            'image' => 'lego1.jpeg',
        ];
        return view('compra/confirmar')->with('products', $products)->with('shipping', $shipping);
    }
}
