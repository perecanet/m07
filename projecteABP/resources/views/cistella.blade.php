@extends('layouts.app')
@section('content')
<div class="all-title-box" style="background-image: url('./images/portada.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Cistella</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Botiga</a></li>
                    <li class="breadcrumb-item active">Cistella</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End All Title Box -->

<!-- Start Cart  -->
<div class="cart-box-main">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-main table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Producte</th>
                                <th>Preu</th>
                                <th>Quantitat</th>
                                <th>Total</th>
                                <th>Esborra</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="thumbnail-img">
                                    <a href="#">
                                <img class="img-fluid" src="images/img-pro-01.jpg" alt="" />
                            </a>
                                </td>
                                <td class="name-pr">
                                    <a href="#">
                                Menú 3
                            </a>
                                </td>
                                <td class="price-pr">
                                    <p>7.50€</p>
                                </td>
                                <td class="quantity-box"><input type="number" size="4" value="1" min="0" step="1" class="c-input-text qty text"></td>
                                <td class="total-pr">
                                    <p>7.50€</p>
                                </td>
                                <td class="remove-pr">
                                    <a href="#">
                                <i class="fas fa-times"></i>
                            </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="thumbnail-img">
                                    <a href="#">
                                <img class="img-fluid" src="images/img-pro-02.jpg" alt="" />
                            </a>
                                </td>
                                <td class="name-pr">
                                    <a href="#">
                                Coca Cola
                            </a>
                                </td>
                                <td class="price-pr">
                                    <p>1.50€</p>
                                </td>
                                <td class="quantity-box"><input type="number" size="4" value="1" min="0" step="1" class="c-input-text qty text"></td>
                                <td class="total-pr">
                                    <p>1.50€</p>
                                </td>
                                <td class="remove-pr">
                                    <a href="#">
                                <i class="fas fa-times"></i>
                            </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="thumbnail-img">
                                    <a href="#">
                                <img class="img-fluid" src="images/img-pro-03.jpg" alt="" />
                            </a>
                                </td>
                                <td class="name-pr">
                                    <a href="#">
                                Mcflurry Oreo
                            </a>
                                </td>
                                <td class="price-pr">
                                    <p>3.50€</p>
                                </td>
                                <td class="quantity-box"><input type="number" size="4" value="1" min="0" step="1" class="c-input-text qty text"></td>
                                <td class="total-pr">
                                    <p>3.50€</p>
                                </td>
                                <td class="remove-pr">
                                    <a href="#">
                                <i class="fas fa-times"></i>
                            </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row my-5">
            <div class="col-lg-6 col-sm-6">
                <div class="coupon-box">
                    <div class="input-group input-group-sm">
                        <input class="form-control" placeholder="Introdueix el codi" aria-label="Coupon code" type="text">
                        <div class="input-group-append">
                            <button class="btn btn-theme" type="button">Aplicar Descompte</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="update-box">
                    <input value="Refrescar Cistella" type="submit">
                </div>
            </div>
        </div>

        <div class="row my-5">
            <div class="col-lg-8 col-sm-12"></div>
            <div class="col-lg-4 col-sm-12">
                <div class="order-box">
                    <h3>Comanda Total</h3>
                    <div class="d-flex">
                        <h4>Sub Total</h4>
                        <div class="ml-auto font-weight-bold"> 12.50€ </div>
                    </div>
                    <div class="d-flex">
                        <h4>Descompte</h4>
                        <div class="ml-auto font-weight-bold"> 0€ </div>
                    </div>
                    <hr class="my-1">
                    <div class="d-flex">
                        <h4>IVA</h4>
                        <div class="ml-auto font-weight-bold"> 2.63€ </div>
                    </div>
                    <div class="d-flex">
                        <h4>Despeses d'enviament</h4>
                        <div class="ml-auto font-weight-bold"> 1.50€ </div>
                    </div>
                    <hr>
                    <div class="d-flex gr-total">
                        <h5> Total </h5>
                        <div class="ml-auto h5"> 16.63€ </div>
                    </div>
                    <hr> </div>
            </div>
            <div class="col-12 d-flex shopping-box"><a href="checkout.html" class="ml-auto btn hvr-hover">Finalitza Pagament</a> </div>
        </div>

    </div>
</div>
    <!-- End Cart -->
@endsection