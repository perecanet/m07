<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\SpaceService;

class SpaceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\NoSpaceService', function ($app) {
            return new SpaceService;
        });

    }


    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
