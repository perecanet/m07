var mongoose = require("mongoose"),
    Docente = require("../models/docente");

//Listar todo
exports.load = async() => {
    var res = await Docente.find({});
    return res;
};
//Guardar
exports.save = (req)=>{
    var newDocente = new Docente(req);
    newDocente.save((err,res)=>{
        if(err) console.log(err);
        else console.log("Insertat a la DB");
        return res;
    });
};
//Listar una
exports.findOne = async(id) => {  
    var res = await Docente.findById(id);
    return res;
};
//Borrar
exports.delete = async(id) => {
    var res = await Docente.findByIdAndRemove(id);
    return res;
};
//Modificar
exports.modify = async(idmod, modificado)=>{
    var res = await Docente.findOneAndUpdate(idmod, modificado);
    return res;
};