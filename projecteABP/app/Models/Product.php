<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function new(){
        $new = new Product;
        $new->name = 'Hamburguesa';
        $new->price  = 11.50;
        $new->category = 'Hamburgueses';
        $new->description  = 'Hamburguesa doble';
        $new->image = 'imatge_burger.jpg';
        $new->save();
    }
}
