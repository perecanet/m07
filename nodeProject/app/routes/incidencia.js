var express = require("express");
var router = express.Router();
var api = require("/app/app/api/request");

router.get("/nova",function (req, res, next){
    res.render("novaincidencia",  {title: "Express"});
});

router.post("/nova", (req, res, next) => {

    api.call("/incidencies/nova",'POST',req.query)
        .then(function (val) {
            res.render("incidencies", val);
        })
        .catch(function (err) {
            console.log(err);
        });
    console.log("se esta procesando");
});

router.get("/llista",function (req, res, next){
    res.render("llistaincidencies",  {title: "Express"});
});

module.exports = router;

