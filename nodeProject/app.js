require("dotenv").config();

var port = process.env.SERVER_PORT;
var express = require("express");
var app = express();
var path = require("path");

var server = require("http").createServer(app).listen(port,()=>{
    console.log("Port: " + port)
});

app.set("views", __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.static(__dirname + '/public'));

var indexRouter = require("./app/routes/base");
var chatRouter = require("./app/routes/chat");
var incidenciaRoutes = require("./app/routes/incidencia");
var apiRouter = require("./app/api/app");

app.use("/", indexRouter);
app.use("/chat", chatRouter);
app.use("/incidencia", incidenciaRoutes);
app.use("/api", apiRouter);