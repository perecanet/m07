var mongoose = require('mongoose'),
    Schema= mongoose.Schema;

    var asigSchema = new Schema({
        nombre: {type:String},
        numHoras: {type:String},
        docente: {type:Schema.Types.ObjectId},
        alumnos: {type:[Schema.Types.ObjectId]}
    });

module.exports = mongoose.model("Asignatura", asigSchema);