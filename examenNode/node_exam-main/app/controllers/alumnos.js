var mongoose = require("mongoose"),
    Alumnos = require("../models/alumnos");

//Listar todo
exports.load = async() => {
    var res = await Alumnos.find({});
    return res;
};
//Guardar
exports.save = (req)=>{
    var newAlumnos = new Alumnos(req);
    newAlumnos.save((err,res)=>{
        if(err) console.log(err);
        else console.log("Insertat a la DB");
        return res;
    });
};
//Listar una
exports.findOne = async(id) => {  
    var res = await Alumnos.findById(id);
    return res;
};
//Borrar
exports.delete = async(id) => {
    var res = await Alumnos.findByIdAndRemove(id);
    return res;
};
//Modificar
exports.modify = async(idmod, modificado)=>{
    var res = await Alumnos.findOneAndUpdate(idmod, modificado);
    return res;
};