<?php

namespace App\Exceptions;

use Exception;

class UploadFileException extends Exception
{
    public function customMessage(){
        return 'No has afegit cap imatge';
    }
}