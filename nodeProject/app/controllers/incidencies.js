exports.loadIncidencies = (req, res, next) => {
    var incidencies = [
        {
            id: '1',
            nombre: 'incidencia1',
            usuario: '/api/usuario?id=101',
            dni: '41748211Z',
            tipo: 'Servidor',
            urgencia: 'baja',
            descripcio: 'error de …'
        },
        {
            id: '2',
            nombre: 'incidencia2',
            usuario: '/api/usuario?id=102',
            dni: '41756723R',
            tipo: 'Problemes de permisos',
            urgencia: 'mitjana',
            descripcio: 'error de …'
        }
    ];
    res.json(incidencies);
};
