var path = require("path");
var ctrlDir = "/app/app/controllers";
var express = require("express");
var router = express.Router();
// include docentes controller
var docentesCtrl = require(path.join(ctrlDir, "docentes"));
var alumnosCtrl = require(path.join(ctrlDir, "alumnos"));

//asignatura

router.get("/asignatura/new", async function (req, res, next) {
    var myDataDoc = await docentesCtrl.load();
    var myDataAlum = await alumnosCtrl.load();
    res.render("newAsignatura", {'myDataDoc': myDataDoc, 'myDataAlum': myDataAlum});
});


// routes chat
router.get("/", function (req, res, next) {
    res.render("chat", { title: "Express" });
});
router.get("/chat", function (req, res, next) {
    res.render("chat", { title: "Express" });
});
router.get("/chat/*", function (req, res, next) {
    var sala = req.originalUrl.slice(-1);
    console.log(sala);
    res.render("chat", { title: "Express", sala:sala });
});

module.exports = router;
