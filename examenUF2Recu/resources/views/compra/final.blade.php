@extends((Request::ajax()) ? 'layouts.ajax' : 'layouts.app')

@section('content')
<div class="content">
    <div class="row">
        <h1>Compra finalizada con éxito, muchas gracias!</h1>
    </div>
</div>
@endsection