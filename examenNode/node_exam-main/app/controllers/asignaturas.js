var mongoose = require("mongoose"),
    Asignatura = require("../models/asignatura");

//Listar todo
exports.load = async() => {
    var res = await Asignatura.find({});
    return res;
};
//Guardar
exports.save = (req)=>{
    var newAsignatura = new Asignatura(req);
    newAsignatura.save((err,res)=>{
        if(err) console.log(err);
        else console.log("Insertat a la DB");
        return res;
    });
};
//Listar una
exports.findOne = async(id) => {  
    var res = await Asignatura.findById(id);
    return res;
};
//Borrar
exports.delete = async(id) => {
    var res = await Asignatura.findByIdAndRemove(id);
    return res;
};
//Modificar
exports.modify = async(idmod, modificado)=>{
    var res = await Asignatura.findOneAndUpdate(idmod, modificado);
    return res;
};