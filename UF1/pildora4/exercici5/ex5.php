<html>
  <head>
    <title>Ex5</title>
    <link rel="stylesheet" href="estil.css">
  </head>
  <body>
    <?php
        function relacion($a, $b) {
            if ($a > $b) {
                return 1;
            } else if ($a < $b) {
                return -1;
            } else {
                return 0;
            }
        }
        $num1 = 3;
        $num2 = 4;
        $resultat = relacion($num1, $num2);
        echo $resultat;
    ?>
  </body>
</html>