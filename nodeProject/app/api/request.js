const request = require("request");

exports.call = function (URI = "", method = "GET", json = {}) {

    const requestOptions = {
        url: "http://localhost:4000/api" + URI,
        method: method,
        json: json,
        qs: {
            offset: 20,
        },
    };
    //PROMISE
    return new Promise(function (resolve, reject) {
        request(requestOptions, (err, response, body) => {
            try {
                resolve(body);
            } catch (e) {
                reject(e);
            }
        });
    });
};