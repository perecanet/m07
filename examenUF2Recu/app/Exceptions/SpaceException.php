<?php

namespace App\Exceptions;

use Exception;

class SpaceException extends Exception
{
    public function customMessage(){
        return 'Espacio insuficiente para guardar la imagen!';
    }

}
