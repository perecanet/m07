var path = require("path");
var ctrlDir = "/app/app/controllers";
var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var bodyParser = require('body-parser');
var urlencode = bodyParser.urlencoded({extended: false});
//Controladores
var asignaturaCtrl = require(path.join(ctrlDir, "asignaturas"));
var docenteCtrl = require(path.join(ctrlDir, "docentes"));
var alumnoCtrl = require(path.join(ctrlDir, "alumnos"));

//save asignatura
router.route('/asignatura/save').post((req, res, next) => {

    var newAsignatura = {
        nombre: req.query.name,
        numHoras: req.query.numHoras,
        docente: req.query.docente,
        alumnos: req.query.alumnos
    };
    console.log("This is the data of asignatura");
    console.log(newAsignatura);
    asignaturaCtrl.save(newAsignatura);
    res.send("Guardado");
})

//findOne asignatura
router.route("/asignatura/:id").get(async(req, res, next) => {
    var id = req.params.id;
    var result = await asignaturaCtrl.findOne(id);
    res.jsonp(result);
})

//get all assignatura
router.route("/asignatura").get(async(req, res, next) => {
    var result = await asignaturaCtrl.load();
    res.jsonp(result);
})

//Eliminar assignatura
router.route( "/asignatura/remove/:id").delete(async (req,res,next) => {
    var id = req.params.id;
    console.log(id);
    var res = await asignaturaCtrl.delete(id);
    console.log("data : "+ res);
    if (!res) {
        res.status(404).send({
          message: 'No se ha encontrado la asignatura'
        });
    } else {
        res.send({
          message: "La asignatura se ha eliminado correctamente"
        });
    }
})
//modify asignatura
router.put( "/asignatura/modify/:id", urlencode,async (req,res,next) => {
    const idmodif = req.params.id;
    console.log(idmodif);
    console.log(req.body);
    var data = await asignaturaCtrl.modify(idmodif,req.body);
    console.log("data : "+data);
    if (!data) {
        res.status(404).send({
          message: 'error'
        });
      } else {
        res.send({
          message: "assignatura modificada"
        });
      }

})

//DOCENTE
//save DOCENTE
router.route('/docente/save').post((req, res, next) => {

    var newDocente = {
        nombre: req.query.name,
        apellido: req.query.apellido
    };
    docenteCtrl.save(newDocente);
    res.send("Guardado");
})

//findOne DOCENTE
router.route("/docente/:id").get(async(req, res, next) => {
    var id = req.params.id;
    var result = await docenteCtrl.findOne(id);
    res.jsonp(result);
})

//get all DOCENTE
router.route("/docente").get(async(req, res, next) => {
    var result = await docenteCtrl.load();
    res.jsonp(result);
})

//Eliminar DOCENTE
router.route( "/docente/remove/:id").delete(async (req,res,next) => {
    var id = req.params.id;
    var res = await docenteCtrl.delete(id);
    if (!res) {
        res.status(404).send({
          message: 'No se ha encontrado la docente'
        });
    } else {
        res.send({
          message: "La docente se ha eliminado"
        });
    }
})
//modify DOCENTE
router.put( "/docente/modify/:id", urlencode,async (req,res,next) => {
    const idmodif = req.params.id;
    var data = await docenteCtrl.modify(idmodif,req.body);
    if (!data) {
        res.status(404).send({
          message: 'error'
        });
      } else {
        res.send({
          message: "docente modificada"
        });
      }

})

//ALUMNOS
//save alumno
router.route('/alumno/save').post((req, res, next) => {

    var newAlumno = {
        nombre: req.query.name,
        apellido: req.query.apellido
    };
    alumnoCtrl.save(newAlumno);
    res.send("Guardado");
})

//findOne alumno
router.route("/alumno/:id").get(async(req, res, next) => {
    var id = req.params.id;
    var result = await alumnoCtrl.findOne(id);
    res.jsonp(result);
})

//get all alumno
router.route("/alumno").get(async(req, res, next) => {
    var result = await alumnoCtrl.load();
    res.jsonp(result);
})

//Eliminar alumno
router.route( "/alumno/remove/:id").delete(async (req,res,next) => {
    var id = req.params.id;
    var res = await alumnoCtrl.delete(id);
    if (!res) {
        res.status(404).send({
          message: 'No se ha encontrado la alumno'
        });
    } else {
        res.send({
          message: "La alumno se ha eliminado"
        });
    }
})
//modify alumno
router.put( "/alumno/modify/:id", urlencode,async (req,res,next) => {
    const idmodif = req.params.id;
    var data = await alumnoCtrl.modify(idmodif,req.body);
    if (!data) {
        res.status(404).send({
          message: 'error'
        });
      } else {
        res.send({
          message: "alumno modificada"
        });
      }

})
module.exports = router;
