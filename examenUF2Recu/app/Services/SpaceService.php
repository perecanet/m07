<?php

namespace App\Services;

use App\Exceptions\SpaceException;

class SpaceService
{
    function spaceleft($file)
    {
        $df = disk_free_space("/");
        $result = $df - $file->getSize();

        if($result < 0){
            throw new SpaceException();
        }
    }
}
